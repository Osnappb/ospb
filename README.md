# Venues where we worked so far  #

The followings are list of venues where we have worked so far by providing our premium service for photo booth. 

### [Hanover Marriott Wedding](https://www.osnapphotobooth.com/single-post/Ami-and-Vivek-The-Hanover-Marriott-Indian-Wedding) ###

* Very Classy and Massive ballroom 
* set up was done inside the venue and in hallway
* so far, coverage for three weddings was provided 
* backdrop used: Navy Blue Sequin backdrop, Mermaid double sided sequin backdrop, gold sequin backdrop
* premium package was offered to all wedding clients

### [The Legacy Castle](https://www.osnapphotobooth.com/single-post/ravi-mansi-the-legacy-castle-prompton-plains-nj-indian-wedding-photo-booth) ###

* Castle, Giant poles, Royal ballroom 
* backdrop used: double sided sequin backdrop (Gold to Black vice versa)
* 3 hours of coverage 
* Social Media Integration was included 
* set up was done in hallway
* Red Carpet step and repeat style set up


### [Westmount Country Club](https://www.osnapphotobooth.com/single-post/Jerin-Meryl-Westmount-Country-Club-Wedding-Photo-Booth) ###

* newly renovated
* plenty of outdoor space for photo shoot
* indoor set up
* backdrop used was navy blue sequin 
* coverage was for entire reception ceremony
* guest loved the GIF coverage provided through iPad photo booth
* social media integration was included 


### [The Grandview](https://www.osnapphotobooth.com/single-post/Wedding-guests-love-our-Photo-Booth) ###

* located in Poughkeepsie, NY
* one elegant venue with hudson river view 
* backdrop used: Gold Sequin backdrop
* set up was outdoor in massive tent 

visit us at https://www.osnapphotobooth/blog for more wedding blogs 
